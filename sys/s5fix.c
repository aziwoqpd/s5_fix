#include <ntddk.h>
#include <wdf.h>
#include <kbdmou.h>
#include "s5fix.h"
#include "public.h"

#pragma warning(push)
#pragma warning(disable:4055) // type case from PVOID to PSERVICE_CALLBACK_ROUTINE
#pragma warning(disable:4152) // function/data pointer conversion in expression

DRIVER_INITIALIZE DriverEntry;
EVT_WDF_DRIVER_DEVICE_ADD rotAddDevice;
EVT_WDF_IO_QUEUE_IO_INTERNAL_DEVICE_CONTROL rotIoctlInternal;
EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL rotcdoIoctl;
EVT_WDF_DEVICE_CONTEXT_CLEANUP rotCleanup;
NTSTATUS rotcdoCreate( IN WDFDEVICE FilterDevice );
void rotcdoDestroy( IN WDFDEVICE FilterDevice );

void rotServiceCallback(
    IN PDEVICE_OBJECT DeviceObject,
    IN PMOUSE_INPUT_DATA InputDataStart,
    IN PMOUSE_INPUT_DATA InputDataEnd,
    IN OUT PULONG InputDataConsumed
    );

#pragma alloc_text( INIT, DriverEntry )
#pragma alloc_text( PAGE, rotAddDevice )
#pragma alloc_text( PAGE, rotIoctlInternal )
#pragma alloc_text( PAGE, rotcdoIoctl )
#pragma alloc_text( PAGE, rotcdoCreate )
#pragma alloc_text( PAGE, rotcdoDestroy )

static WDFCOLLECTION RotDeviceCollection;
static WDFWAITLOCK RotDeviceCollectionLock;
static WDFDEVICE RotControlDevice;

NTSTATUS DriverEntry( IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath )
{
    NTSTATUS status = STATUS_SUCCESS;
    WDF_DRIVER_CONFIG config;

    PAGED_CODE();

    KdPrint(( "s5fix DriverEntry\n" ));
    WDF_DRIVER_CONFIG_INIT( &config, rotAddDevice );
    config.DriverPoolTag = ' tor';
    
    status = WdfDriverCreate( DriverObject, RegistryPath, NULL, &config, WDF_NO_HANDLE );
    if( !NT_SUCCESS( status ) ) return status;

    status = WdfCollectionCreate( WDF_NO_OBJECT_ATTRIBUTES, &RotDeviceCollection );
    if( !NT_SUCCESS( status ) ) return status;

    status = WdfWaitLockCreate( WDF_NO_OBJECT_ATTRIBUTES, &RotDeviceCollectionLock );
    if( !NT_SUCCESS( status ) ) return status;

    KdPrint(( "DriverEntry OK..\n" ));

    return status;
}

NTSTATUS rotAddDevice( IN WDFDRIVER Driver, IN PWDFDEVICE_INIT DeviceInit )
{
    WDF_OBJECT_ATTRIBUTES attr;
    NTSTATUS status;
    WDFDEVICE filterDevice;
    WDF_IO_QUEUE_CONFIG ioQueueConfig;
    PROTFIX_DEVICE_EXTENSION extension;

    UNREFERENCED_PARAMETER( Driver );

    PAGED_CODE();
    KdPrint(( "rotAddDevice\n" ));

    // Forward IRPs to next device
    WdfFdoInitSetFilter( DeviceInit );

    WdfDeviceInitSetDeviceType( DeviceInit, FILE_DEVICE_MOUSE );

    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE( &attr, ROTFIX_DEVICE_EXTENSION );
    attr.EvtCleanupCallback = rotCleanup;

    status = WdfDeviceCreate( &DeviceInit, &attr, &filterDevice );
    if( !NT_SUCCESS(status) ) {
        return status;
    }

    extension = WdfObjectGet_ROTFIX_DEVICE_EXTENSION( filterDevice );

    // Create queue for the filter

    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE( &ioQueueConfig, WdfIoQueueDispatchParallel );
    ioQueueConfig.EvtIoInternalDeviceControl = rotIoctlInternal;

    status = WdfIoQueueCreate( filterDevice, &ioQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE );
    if( !NT_SUCCESS( status ) ) {
        return status;
    }

    // Create queue for the CDO
    status = rotcdoCreate( filterDevice );

    if( !NT_SUCCESS( status ) ) {
        return status;
    }

    WdfWaitLockAcquire( RotDeviceCollectionLock, NULL );
    status = WdfCollectionAdd( RotDeviceCollection, filterDevice );
    WdfWaitLockRelease( RotDeviceCollectionLock );

    KdPrint(( "AddDevice OK\n" ));

    return status;
}

void rotCleanup( IN WDFDEVICE Device )
{
    PAGED_CODE();

    KdPrint(( "rotCleanup\n" ));
    WdfWaitLockAcquire( RotDeviceCollectionLock, NULL );
    if( WdfCollectionGetCount( RotDeviceCollection ) == 1 ) {
        // last object -> destroy cdo
        rotcdoDestroy( Device );
    }
    WdfCollectionRemove( RotDeviceCollection, Device );
    WdfWaitLockRelease( RotDeviceCollectionLock );
    KdPrint(( "rotCleanup OK\n" ));
}

NTSTATUS rotcdoCreate( IN WDFDEVICE FilterDevice )
{
    BOOLEAN wantToCreate = FALSE;
    PWDFDEVICE_INIT deviceInit = NULL;
    WDFDEVICE cdoDevice = NULL;
    WDF_IO_QUEUE_CONFIG ioQueueConfig;
    NTSTATUS status = STATUS_SUCCESS;

    DECLARE_CONST_UNICODE_STRING(ntDeviceName, L"\\Device\\VilivRotFix") ;
    DECLARE_CONST_UNICODE_STRING(ntDosDeviceName, L"\\DosDevices\\VilivRotFix") ;

    PAGED_CODE();

    KdPrint(( "rotcdoCreate\n" ));

    WdfWaitLockAcquire( RotDeviceCollectionLock, NULL );
    if( WdfCollectionGetCount( RotDeviceCollection ) == 0 ) {
        wantToCreate = TRUE;
    }
    WdfWaitLockRelease( RotDeviceCollectionLock );

    if( !wantToCreate ) return STATUS_SUCCESS;

    deviceInit = 
        WdfControlDeviceInitAllocate( WdfDeviceGetDriver( FilterDevice ), &SDDL_DEVOBJ_SYS_ALL_ADM_RWX_WORLD_RW_RES_R );

    if( deviceInit == NULL ) {
        status = STATUS_INSUFFICIENT_RESOURCES;
        goto cleanup;
    }

    WdfDeviceInitSetExclusive( deviceInit, FALSE );

    status = WdfDeviceInitAssignName( deviceInit, &ntDeviceName );
    if( !NT_SUCCESS(status) ) goto cleanup;

    status = WdfDeviceCreate( &deviceInit, WDF_NO_OBJECT_ATTRIBUTES, &cdoDevice );
    if( !NT_SUCCESS(status) ) goto cleanup;

    status = WdfDeviceCreateSymbolicLink( cdoDevice, &ntDosDeviceName );

    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE( &ioQueueConfig, WdfIoQueueDispatchSequential );
    ioQueueConfig.EvtIoDeviceControl = rotcdoIoctl;
    status = WdfIoQueueCreate( cdoDevice, &ioQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, WDF_NO_HANDLE );
    if( !NT_SUCCESS( status ) ) goto cleanup;

    WdfControlFinishInitializing( cdoDevice );
    RotControlDevice = cdoDevice;

    KdPrint(( "rotcdoCreate OK\n" ));
    return status;

cleanup:
    if( deviceInit != NULL ) WdfDeviceInitFree( deviceInit );
    if( cdoDevice != NULL ) WdfObjectDelete( cdoDevice );
    return status;
}

void rotcdoDestroy( IN WDFDEVICE Device )
{
    UNREFERENCED_PARAMETER( Device );
    PAGED_CODE();
    KdPrint(( "rotDestroy\n" ));
    if( RotControlDevice != NULL ) WdfObjectDelete( RotControlDevice );
    RotControlDevice = NULL;
}

void rotcdoIoctl(
    WDFQUEUE Queue,
    WDFREQUEST Request,
    size_t OutputBufferLength,
    size_t InputBufferLength,
    ULONG IoControlCode )
{
    ULONG i, count;
    WDFDEVICE filterDevice;
    PROTFIX_DEVICE_EXTENSION ext;
    NTSTATUS status;
    S5FIX_CHANGE_ROTATION_IN *cmd;

    UNREFERENCED_PARAMETER( Queue );
    UNREFERENCED_PARAMETER( OutputBufferLength );
    UNREFERENCED_PARAMETER( InputBufferLength );

    PAGED_CODE();

    KdPrint(( "rotcdoIoctl\n" ));

    if( IoControlCode != IOCTL_S5FIX_CHANGE_ROTATION ) {
        KdPrint(( "ioctl unknown\n" ));
        WdfRequestComplete( Request, STATUS_NOT_IMPLEMENTED );
        return;
    }
    status = WdfRequestRetrieveInputBuffer( Request, sizeof( S5FIX_CHANGE_ROTATION_IN ), &cmd, NULL );
    if( !NT_SUCCESS( status ) ) {
        WdfRequestComplete( Request, status );
        return;
    }

    WdfWaitLockAcquire( RotDeviceCollectionLock, NULL );
    count = WdfCollectionGetCount( RotDeviceCollection );
    for( i=0 ; i<count ; i++ ) {
        KdPrint(( "ioctl adjusts filter\n" ));
        filterDevice = WdfCollectionGetItem( RotDeviceCollection, i );
        ext = WdfObjectGet_ROTFIX_DEVICE_EXTENSION( filterDevice );        
        ext->Rotation = cmd->angle;
    }
    WdfWaitLockRelease( RotDeviceCollectionLock );
    WdfRequestComplete( Request, STATUS_SUCCESS );
    KdPrint(( "ioctl OK\n" ));
}

void rotIoctlInternal(
    IN WDFQUEUE      Queue,
    IN WDFREQUEST    Request,
    IN size_t        OutputBufferLength,
    IN size_t        InputBufferLength,
    IN ULONG         IoControlCode
    )
{
    WDFDEVICE filterDevice = WdfIoQueueGetDevice( Queue );
    WDF_REQUEST_SEND_OPTIONS options;
    NTSTATUS status = STATUS_SUCCESS;
    PROTFIX_DEVICE_EXTENSION extension = WdfObjectGet_ROTFIX_DEVICE_EXTENSION( filterDevice );
    PCONNECT_DATA connectData;
    size_t length;

    UNREFERENCED_PARAMETER( InputBufferLength );
    UNREFERENCED_PARAMETER( OutputBufferLength );

    PAGED_CODE();

    KdPrint(( "ioctlInternal\n" ));
    
    switch( IoControlCode ) {
    case IOCTL_INTERNAL_MOUSE_CONNECT:
        // Attach the filter function
        KdPrint(( "ioctlConnect\n" ));
        if( extension->UpperConnectData.ClassService != NULL ) {
            status = STATUS_SHARING_VIOLATION;
            break;
        }
        status = WdfRequestRetrieveInputBuffer( Request, sizeof(CONNECT_DATA), &connectData, &length );
        if( !NT_SUCCESS( status ) ) {
            break;
        }
        extension->UpperConnectData = *connectData;
        connectData->ClassDeviceObject = WdfDeviceWdmGetDeviceObject( filterDevice );
        connectData->ClassService = &rotServiceCallback;
        break;
    }

    if( !NT_SUCCESS( status ) ) {
        WdfRequestComplete( Request, status );
        return;
    }

    // Pass to base class after we're done
    WDF_REQUEST_SEND_OPTIONS_INIT( &options, WDF_REQUEST_SEND_OPTION_SEND_AND_FORGET );
    if( !WdfRequestSend( Request, WdfDeviceGetIoTarget( filterDevice ), &options ) ) {
        status = WdfRequestGetStatus( Request );
        WdfRequestComplete( Request, status );
    }    
    KdPrint(( "ioctl OK\n" ));
}

void rotServiceCallback(
    IN PDEVICE_OBJECT DeviceObject,
    IN PMOUSE_INPUT_DATA InputDataStart,
    IN PMOUSE_INPUT_DATA InputDataEnd,
    IN OUT PULONG InputDataConsumed
    )
{
    WDFDEVICE filterDevice = WdfWdmDeviceGetWdfDeviceHandle( DeviceObject );
    PROTFIX_DEVICE_EXTENSION extension = WdfObjectGet_ROTFIX_DEVICE_EXTENSION( filterDevice );
    int rotation = extension->Rotation;
    PMOUSE_INPUT_DATA InputCurrent;

    KdPrint(( "srv callback with rotation %d\n", rotation ));

    if( rotation == 90 ) {
        for( InputCurrent = InputDataStart ; InputCurrent < InputDataEnd ; InputCurrent++ ) {
            LONG x = InputCurrent->LastX;
            LONG y = InputCurrent->LastY;

            if( InputCurrent->Flags & MOUSE_MOVE_ABSOLUTE ) {
                InputCurrent->LastX = 65535 - y;
                InputCurrent->LastY = x;
            } else {
                InputCurrent->LastX = -y;
                InputCurrent->LastY = x;
            }
        }
    } else if( rotation == 180 ) {
        for( InputCurrent = InputDataStart ; InputCurrent < InputDataEnd ; InputCurrent++ ) {
            LONG x = InputCurrent->LastX;
            LONG y = InputCurrent->LastY;
    
            if( InputCurrent->Flags & MOUSE_MOVE_ABSOLUTE ) {
                InputCurrent->LastX = 65535 - x;
                InputCurrent->LastY = 65535 - y;
            } else {
                InputCurrent->LastX = -x;
                InputCurrent->LastY = -y;
            }
        }
    } else if( rotation == 270 ) {
        for( InputCurrent = InputDataStart ; InputCurrent < InputDataEnd ; InputCurrent++ ) {
            LONG x = InputCurrent->LastX;
            LONG y = InputCurrent->LastY;
    
            if( InputCurrent->Flags & MOUSE_MOVE_ABSOLUTE ) {
                InputCurrent->LastX = y;
                InputCurrent->LastY = 65535 - x;
            } else {
                InputCurrent->LastX = y;
                InputCurrent->LastY = -x;
            }
        }
    }
    
    (*(PSERVICE_CALLBACK_ROUTINE) extension->UpperConnectData.ClassService)(
        extension->UpperConnectData.ClassDeviceObject,
        InputDataStart,
        InputDataEnd,
        InputDataConsumed
        );
}

#pragma warning(pop)
