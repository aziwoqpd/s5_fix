/**
 * Viliv S5 rotation fixer 
 * by Jim Perry 
 */

#ifndef S5FIXER_H
#define S5FIXER_H

#ifdef DBG
#define KdPrint(_x_) DbgPrint _x_
#else
#define KdPrint(_x_)
#endif

typedef struct _ROTFIX_DEVICE_EXTENSION {
    CONNECT_DATA UpperConnectData;
    int Rotation;
    WDFQUEUE CdoQueue;
} ROTFIX_DEVICE_EXTENSION, *PROTFIX_DEVICE_EXTENSION;

WDF_DECLARE_CONTEXT_TYPE( ROTFIX_DEVICE_EXTENSION )

#endif  // S5FIXER_H
