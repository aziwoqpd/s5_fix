// display.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <windows.h>
#include <winioctl.h>
#include <ntsecapi.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../sys/public.h"

void DriverUpdate(int angle)
{
    HANDLE h = CreateFileW( L"\\\\.\\VilivRotFix", GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL );
    if( h == INVALID_HANDLE_VALUE ) {
        MessageBoxW( NULL, L"Error communication with Viliv touch driver", L"Error", MB_TOPMOST );
        return;
    }

    DWORD ret;
    S5FIX_CHANGE_ROTATION_IN cmd;
    cmd.angle = angle;

    DeviceIoControl( h, IOCTL_S5FIX_CHANGE_ROTATION, &cmd, sizeof(cmd), NULL, 0, &ret, NULL );

    CloseHandle(h);
}

void ResolutionChanged()
{
    DEVMODE dm;

    ZeroMemory( &dm, sizeof( DEVMODE ) );
    dm.dmSize = sizeof( DEVMODE );
    EnumDisplaySettingsEx( NULL, ENUM_CURRENT_SETTINGS, &dm, 0 );

    switch(dm.dmDisplayOrientation) {
    case DMDO_90:
        DriverUpdate(90);
        //MessageBoxW( NULL, L"Change 90", L"Orientation", MB_TOPMOST );
        break;
    case DMDO_180:
        DriverUpdate(180);
        //MessageBoxW( NULL, L"Change 180", L"Orientation", MB_TOPMOST );
        break;
    case DMDO_270:
        DriverUpdate(270);
        //MessageBoxW( NULL, L"Change 270", L"Orientation", MB_TOPMOST );
        break;
    default:
        DriverUpdate(0);
        //MessageBoxW( NULL, L"Change 0", L"Orientation", MB_TOPMOST );
        break;
    }
}

LRESULT CALLBACK DummyWndProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch( uMsg ) {
    case WM_DISPLAYCHANGE:
        ResolutionChanged();
        return 0;
    default: return DefWindowProc( hwnd, uMsg, wParam, lParam );
    }
}

int WINAPI WinMain( HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow
)
{
    HANDLE runMutex = CreateMutexW( NULL, FALSE, L"S5FixRunning" );
    if( GetLastError() == ERROR_ALREADY_EXISTS ) return 0;      // Don't run more than once
    CloseHandle( runMutex );

    // Create a dummy window so we can receive WM_DISPLAYCHANGED
    WNDCLASSEXW wex;
    ZeroMemory( &wex, sizeof(wex) );
    wex.cbSize = sizeof(WNDCLASSEXW);
    wex.hInstance = hInstance;
    wex.lpfnWndProc = DummyWndProc;
    wex.lpszClassName = L"dummywnd";
    ATOM dummyClass = RegisterClassExW( &wex );

    HWND hwndDummy = 
        CreateWindowW( (LPCWSTR) dummyClass, L"S5Fix", WS_OVERLAPPEDWINDOW, 0, 0, 100, 100, NULL, NULL, hInstance, NULL );

    MSG msg;

    ResolutionChanged();

    while( GetMessage( &msg, NULL, 0, 0 ) ) {
        TranslateMessage( &msg );
        DispatchMessage( &msg );
    }
    return 0;
}
